export class NggPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('ngg-app h1')).getText();
  }
}
