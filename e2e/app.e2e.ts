import { NggPage } from './app.po';

describe('ngg App', function() {
  let page: NggPage;

  beforeEach(() => {
    page = new NggPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('ngg works!');
  });
});
