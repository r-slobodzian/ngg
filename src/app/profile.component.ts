import { Component } from '@angular/core';
import { CourseService } from './services/course.service';
import { AutoGrowDirective } from './directives/auto-grow.directive';

@Component({
	selector: 'profile',
	template: `
		<h2>Profile {{ title }}</h2>
		<input type="text" autoGrow />
		<ul>
			<li *ngFor="#course of courses">
				{{ course }}
			</li>
		</ul>
		`,
	providers: [CourseService],
	directives: [AutoGrowDirective]
})
export class ProfileComponent {
	title = "Test";
 	courses;

	constructor(courseService: CourseService) {
		this.courses = courseService.getGourses();
	}
}