import {Ship} from "../models/ship";
import {MeshesFactory} from "../factory/meshes.factory";
import {WhizzbangService} from "./whizzbang.service";

export class ShipControlService {
    ship:Ship;
    whizzbangService:WhizzbangService;

    constructor(ship:Ship, meshesFactory:MeshesFactory) {
        this.ship = ship;
        this.whizzbangService = new WhizzbangService(this.ship, meshesFactory);

        BABYLON.Tools.RegisterTopRootEvents([{
            name: "keydown",
            handler: this.onKeyDown.bind(this)
        }, {
            name: "keyup",
            handler: this.onKeyUp.bind(this)
        }]);
    }

    private onKeyDown(e) {
        var key = e.keyCode ? e.keyCode : e.which;
        console.log(key);
        if (key == 87) {
            this.ship.speed = 0.15;
        } else if (key == 65) {
            this.ship.moveLeft = true;
            this.ship.moveRight = false;
        } else if (key == 68) {
            // To the right
            this.ship.moveRight = true;
            this.ship.moveLeft = false;
        }
    }

    private onKeyUp(e) {
        var key = e.keyCode ? e.keyCode : e.which;
        console.log(key);

        if (key == 87) {
            this.ship.speed = 0;
        } else if (key == 65) {
            this.ship.moveLeft = false;
        } else if (key == 68) {
            this.ship.moveRight = false;
        }
    }
}