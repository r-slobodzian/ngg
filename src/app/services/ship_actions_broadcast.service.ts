import {Ship} from "../models/ship";

export class ShipActionsBroadcastService {
    ship:Ship;
    socket;

    constructor(socket, ship:Ship) {
        this.ship = ship;
        this.socket = socket;
    }

    move() {
        this.socket.emit('playerMove', this.ship.serialize());
    }
}