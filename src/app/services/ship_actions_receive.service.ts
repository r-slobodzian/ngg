import {Ship} from "../models/ship";

export class ShipActionsReceiveService {
    ship:Ship;
    socket;
    ss = 0;

    constructor(socket, ship:Ship) {
        this.ship = ship;
        this.socket = socket;

        this.socket.on('playerMove' + this.ship.id, this.shipMove.bind(this));
    }

    destroy() {
        this.ship.dispose();
    }

    private shipMove(data) {
        this.ship.setState(data)
    }
}