import {
  beforeEachProviders,
  describe,
  expect,
  it,
  inject
} from '@angular/core/testing';
import { NggAppComponent } from '../app/ngg.component';

beforeEachProviders(() => [NggAppComponent]);

describe('App: Ngg', () => {
  it('should create the app',
      inject([NggAppComponent], (app: NggAppComponent) => {
    expect(app).toBeTruthy();
  }));

  it('should have as title \'ngg works!\'',
      inject([NggAppComponent], (app: NggAppComponent) => {
    expect(app.title).toEqual('ngg works!');
  }));
});
