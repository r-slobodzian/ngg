export class Ship {
    scene;
    ship:BABYLON.Mesh;
    moveRight;
    moveLeft;
    speed = 0;
    id = 0;

    constructor(mesh) {
        this.ship = mesh;
    }

    setId(id = null) {
        if(id) {
            this.id = id;
        } else {
            this.id = Math.round((new Date).getTime() + Math.random() * 10000000000000)
        }
    }

    move() {
        this.ship.translate(BABYLON.Axis.Z, this.speed, BABYLON.Space.LOCAL);

        if (this.moveRight) {
            this.ship.rotate(BABYLON.Axis.Y, 0.025, BABYLON.Space.LOCAL);
        }
        if (this.moveLeft) {
            this.ship.rotate(BABYLON.Axis.Y, -0.025, BABYLON.Space.LOCAL);
        }

    }

    dispose() {
        this.ship.dispose();
    }

    serialize() {
        return {
            id: this.id,
            rotation: this.ship.rotationQuaternion.asArray(),
            position: this.ship.position.asArray()
        }
    }

    setState(state) {
        this.ship.rotationQuaternion = BABYLON.Quaternion.FromArray(state.rotation);
        this.ship.position = BABYLON.Vector3.FromArray(state.position);
    }
}
