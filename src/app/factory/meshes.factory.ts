import {MeshesRepository} from "../repositories/ships";
import {Ship} from "../models/ship";

export class MeshesFactory {
    repository:MeshesRepository;

    constructor(scene, ready) {
        this.repository = new MeshesRepository(scene, ready.bind(null, this)) ;
    }

    getShip(name) : Ship {
        if(name == 'ship') {
            return new Ship(this.repository.getShip(name));
        }
    }
}