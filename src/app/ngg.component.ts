import { Component } from '@angular/core';
import {ROUTER_DIRECTIVES, Routes} from '@angular/router';
import {PresentationComponent} from './components/presentation/presentation'
import {ProfileComponent} from './components/profile/profile'

@Component({
  moduleId: module.id,
  selector: 'ngg-app',
  templateUrl: 'ngg.component.html',
  styleUrls: ['ngg.component.css'],
  directives: [ROUTER_DIRECTIVES]
})
@Routes([
  {path: '/', component: PresentationComponent},
  {path: '/profile', component: ProfileComponent},
])
export class NggAppComponent {
  title = 'Ngg works!';
}
