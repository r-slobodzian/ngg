import {Directive, ElementRef, Renderer} from '@angular/core';

@Directive({
	selector: '[autoGrow]',
	host: {
		'(focus)': 'onFocus()',
		'(blur)': 'onBlur()',
	}
})
export class AutoGrowDirective {

	constructor(private el: ElementRef, private renderer: Renderer) {
	}

	onFocus() {
		console.log('01111');
		this.renderer.setElementStyle(this.el.nativeElement, 'width', '200px');
	}
	
	onBlur() {
		console.log('2222222');
		this.renderer.setElementStyle(this.el.nativeElement, 'width', '150px');
	}
}