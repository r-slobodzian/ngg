import {Component, ViewChild, AfterViewInit} from '@angular/core';
import {ShipControlService} from "../../services/ship_control.service";
import {MeshesFactory} from "../../factory/meshes.factory";
import {ShipActionsBroadcastService} from "../../services/ship_actions_broadcast.service";
import {ShipActionsReceiveService} from "../../services/ship_actions_receive.service";
import {WhizzbangService} from "../../services/whizzbang.service";

declare var io, window: any;

@Component({
    selector: 'game',
    templateUrl: './app/components/game/game.html',
})
export class GameComponent implements AfterViewInit {
    @ViewChild('gameCanvas')
    myCanvas;
    socket;
    camera;
    engine;
    scene;
    ship;
    shipBroadcast;
    meshesFactory:MeshesFactory;
    receives = {};

    ngOnInit() {
        this.socket = io(`${location.protocol}//${location.hostname}:8003`);
    }

    ngAfterViewInit() {
        let canvas = this.myCanvas.nativeElement;

        this.engine = new BABYLON.Engine(canvas, true);
        this.scene = new BABYLON.Scene(this.engine);

        this.scene.enablePhysics();
        this.scene.setGravity(new BABYLON.Vector3(0, -10, 0));

        this.camera = new BABYLON.FollowCamera("FollowCam", new BABYLON.Vector3(0, 300, -1700), this.scene);
        this.camera.radius = 30; // how far from the object to follow
        this.camera.heightOffset = 8; // how high above the object to place the camera
        this.camera.rotationOffset = 180; // the viewing angle
        this.camera.cameraAcceleration = 0.05 // how fast to move
        this.camera.maxCameraSpeed = 20 // speed limit

        this.scene.activeCamera = this.camera;

        var light0 = new BABYLON.DirectionalLight("dir01", new BABYLON.Vector3(-0.5, -1, -0.5), this.scene);

        // var shadowGenerator = new BABYLON.ShadowGenerator(1024, light0);

        var box = BABYLON.Mesh.CreateBox('Box', 3, this.scene);
        box.position.x = -5;
        box.position.z = 150;
        // box.setPhysicsState({impostor: BABYLON.PhysicsEngine.BoxImpostor, mass: 1, friction: 0.5, restitution: 0.7});
        box.receiveShadows = true;

        var ground = BABYLON.Mesh.CreateBox('ground', 50, this.scene);
        ground.position.y = -10;
        ground.scaling.y = 0.1;
        ground.setPhysicsState({impostor: BABYLON.PhysicsEngine.BoxImpostor, mass: 0, friction: 0.5, restitution: 0.7});
        ground.receiveShadows = true;

        // todo think about preload
        // todo preload using MeshesFactory
        new MeshesFactory(this.scene, this.run.bind(this));
    }

    run(meshesFactory:MeshesFactory) {
        this.meshesFactory = meshesFactory;

        this.registerCurrentPlayer();
        
        this.registerOldPlayers();

        this.socket.on('newPlayer', this.registerNewPlayer.bind(this));
        this.socket.on('disconnectPlayer', this.disconnectPlayer.bind(this));

        this.engine.runRenderLoop(() => {
            this.ship.move();
            this.shipBroadcast.move();
            this.scene.render();
        });
    }

    registerCurrentPlayer() {
        this.ship = this.meshesFactory.getShip('ship');
        this.ship.setId();
        new ShipControlService(this.ship, this.meshesFactory);
        this.shipBroadcast = new ShipActionsBroadcastService(this.socket, this.ship);
        this.camera.target = this.ship.ship;
        this.socket.emit("newPlayer", {id: this.ship.id});
    }

    registerOldPlayers() {
        this.socket.emit('getPlayers', (players) => {
            console.log('getPlayers', players);
            players.forEach((player) => {
                if(player.id != this.ship.id) {
                    this.registerNewPlayer(player);
                }
            });
        });
    }

    registerNewPlayer(data) {
        if(!this.receives[data.id]) {
            console.log('new player ', data);
            var ship = this.meshesFactory.getShip('ship');
            ship.setId(data.id);
            this.receives[data.id] = new ShipActionsReceiveService(this.socket, ship);;
        }
    }

    disconnectPlayer(data) {
        console.log('disconnected player ', data);

        this.receives[data.id].destroy();
        delete this.receives[data.id];
    }
}
