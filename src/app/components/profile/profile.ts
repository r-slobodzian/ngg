import { Component } from '@angular/core';
import {ROUTER_DIRECTIVES, Routes} from '@angular/router';
import {GameComponent} from './../game/game'

@Component({
	selector: 'profile',
	templateUrl: './app/components/profile/profile.html',
  	directives: [ROUTER_DIRECTIVES]
})
@Routes([
  	{path: '/game', component: GameComponent},
])
export class ProfileComponent {
}