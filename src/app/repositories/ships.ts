var meshes = [];
var cloned = 0;

export class MeshesRepository {
    constructor(scene, ready) {
        BABYLON.SceneLoader.ImportMesh("", "./bscene/ship2/", "star-wars-vader-tie-fighter.babylon", scene, function (newMeshes, particleSystems) {
            var ship = newMeshes[1];
            ship.setEnabled(false);
            ship.scaling = new BABYLON.Vector3(5, 5, 5);
            meshes['ship'] = ship;
            ready();
        }.bind(this));
    }

    getShip(name) {
        console.log('mesh_' + name + '_' + cloned);
        return meshes[name].clone('mesh_' + name + '_' + cloned++);
    }
}