var express = require('express');
var app = express();
var http = require('http');
var server = http.createServer(app);
var io = require('socket.io').listen(server);

var gameServer = {
    start: function () {
        var players = [];
        server.listen(8003);
        io.set("origins", "*:*");

        io.on('connection', function (socket) {
            var data;
            socket.on('newPlayer', function (dataArray) {
                console.log('New player');
                console.log(players);
                data = dataArray;
                players.push(data);
                socket.broadcast.emit('newPlayer', data);
            });
            
            socket.on('playerMove', function (dataArray) {
                socket.broadcast.emit('playerMove' + dataArray.id, dataArray);
            });

            socket.on('getPlayers', function (fn) {
                console.log('Pet players');
                fn(players);
            });

            socket.on('disconnect', function (fn) {
                console.log('Got disconnect!');

                var i = players.indexOf(data);
                players.splice(i, 1);
                socket.broadcast.emit('disconnectPlayer', data);
            });
        });
    }
};

gameServer.start();

module.exports = gameServer;